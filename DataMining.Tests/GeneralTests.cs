﻿using System;
using System.Collections.Generic;
using System.Text;
using DataMining.Algorithm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataMining.Tests
{
	[TestClass]
	public class ProblemClassTests
	{
		[TestMethod]
		public void G_ProblemClassCreationTest()
		{
			var p = new ProblemClass();
			p.RegisterProperty( "cost" );
			Assert.AreEqual( 0.0f, p.GetProperty( "cost" ) );
		}
		[TestMethod]
		[ExpectedException( typeof( KeyNotFoundException ) )]
		public void G_ProblemClassCrtExcptTest()
		{
			var p = new ProblemClass();
			Assert.AreEqual( 0.0f, p.GetProperty( "cost" ) );
		}
	}

	[TestClass]
	public class TreeNodeTests
	{
		public TreeNode<ProblemClass> GetSampleTree()
		{
			var p = new ProblemClass( "ID" );
			p.SetProperty( "ID", 1 );
			var p2 = new ProblemClass( "ID" );
			p2.SetProperty( "ID", 2 );
			var p3 = new ProblemClass( "ID" );
			p3.SetProperty( "ID", 3 );
			var p4 = new ProblemClass( "ID" );
			p4.SetProperty( "ID", 4 );
			var p5 = new ProblemClass( "ID" );
			p5.SetProperty( "ID", 5 );
			var p6 = new ProblemClass( "ID" );
			p6.SetProperty( "ID", 6 );

			TreeNode<ProblemClass> root = new TreeNode<ProblemClass>( p );
			{

				TreeNode<ProblemClass> node0 = root.AddChild( p2 );
				{
					TreeNode<ProblemClass> node20 = node0.AddChild( p3 );
					TreeNode<ProblemClass> node21 = node0.AddChild( p3 );
					{
						TreeNode<ProblemClass> node210 = node21.AddChild( p4 );
						TreeNode<ProblemClass> node211 = node21.AddChild( p5 );
					}
				}
				TreeNode<ProblemClass> node3 = root.AddChild( p6 );
			}

			return root;
		}

		private String CreateIndent( int depth )
		{
			StringBuilder sb = new StringBuilder();
			for ( int i = 0; i < depth; i++ )
			{
				sb.Append( ' ' );
			}
			return sb.ToString();
		}

		[TestMethod]
		public void G_TreeCreationTest()
		{
			TreeNode<ProblemClass> root = new TreeNode<ProblemClass>( new ProblemClass( "cost" ) );
			//p.RegisterProperty( "cost" );
			//Assert.AreEqual( 0.0f, p.GetProperty( "cost" ) );
			//Assert.IsNotNull( TreeNode<ProblemClass>.Root );
		}

		[TestMethod]
		public void G_TreeTest()
		{
			TreeNode<ProblemClass> root = GetSampleTree();
			//p.RegisterProperty( "cost" );
			//Assert.AreEqual( 0.0f, p.GetProperty( "cost" ) );
			//Assert.IsNotNull( TreeNode<ProblemClass>.Root );

			//TreeNode<string> treeRoot = SampleData.GetSet1();

			StringBuilder s = new StringBuilder();

			foreach ( TreeNode<ProblemClass> node in root )
			{
				s.Append( CreateIndent( node.Level ) + ( node.Data.ToString() ?? "null" ) + "\n" );
			}

			var b = s.ToString();
		}
	}
}
