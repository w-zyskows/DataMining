﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataMining.DB;
using DataMining.DB.HouseDB;
using System.Linq;
using DataMining.DB.DataGenerators;
using System.Collections.Generic;
using DataMining.DB.BillDB;
using DataMining.DB.ExampleDB;

namespace DataMining.Tests
{
	[TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void BasicOperations()
        {
            using (var ctx = new DatabaseContext())
            {
                House house = new House() { InhabitantsNumber = 4 };
                ctx.Houses.Add(house);

                ctx.SaveChanges();

                House newHouse = ctx.Houses.Where(h => h.InhabitantsNumber == 4).FirstOrDefault();
                Assert.IsNotNull(newHouse);
            }
        }

        [TestMethod]
        public void DataGeneration()
        {
            House house = HouseGenerator.createRandomHouse();
            Assert.IsNotNull(house);
            Assert.IsNotNull(house.Age);
            Assert.IsNotNull(house.HeatingType);
            Assert.IsNotNull(house.HouseType);
            Assert.IsNotNull(house.InhabitantsNumber);
            Assert.IsNotNull(house.MaterialType);
            Assert.IsNotNull(house.Size);

            DecisionExample DExample = generateFilledDecisionExample(house);

            Assert.IsNotNull(DExample);
            Assert.IsNotNull(DExample.WaterVendor);
            Assert.IsNotNull(DExample.GasVendor);
            Assert.IsNotNull(DExample.ElectricityVendor);
            Assert.IsNotNull(DExample.ElectricityBills);
            Assert.IsNotNull(DExample.GasBills);
            Assert.IsNotNull(DExample.WaterBills);
            Assert.IsNotNull(DExample.Decision);
        }

        [TestMethod]
        public void DataInsertion()
        {
            TestData.GenerateDatabaseSet(10);
            using (var ctx = new DatabaseContext())
            {
                Assert.IsTrue( ctx.Houses.Count() > 10);
            }
        }
        private static ICollection<Bill> generateBillList(House house, BillType billType, int billCount)
        {
            ICollection<Bill> bills = new List<Bill>();
            for (int i = 0; i < billCount; i++)
            {
                bills.Add(BillGenerator.generateBill(house, billType));
            }
            return bills;
        }

        private static DecisionExample generateFilledDecisionExample(House house)
        {
            int billCount = RG.GetRandomInt(1, 12);
            ICollection<Bill> gasBills = generateBillList(house, BillType.Gas, billCount);
            ICollection<Bill> waterBills = generateBillList(house, BillType.Water, billCount);
            ICollection<Bill> electricityBills = generateBillList(house, BillType.Electricity, billCount);

            return DEGenerator.generateDecisionExample(house, gasBills, waterBills, electricityBills);
        }
    }
}
