﻿using System;
using DataMining.DecisionTree;
using DataMining.Config;
using DataMining.DecisionTree.Data;
using DataMining.DecisionTree.Utils;
using SampleDataParsers.Car;
using SampleDataParsers.Lenses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using DataMining.DB.ExampleDB;
using DataMining.DB.DataGenerators;

namespace DataMining.Tests
{
	[TestClass]
	public class DecisionTreeTest
	{
		const string CAN_SURVIVE_WITHOUT_SURFACING = "can survive without surfacing";
		const string HAS_FLIPPERS = "has flippers";
		const string IS_FISH = "is fish";
		[TestMethod]
		public void TestEntropy()
		{
			var entropy = Decider.Entropy( GetDataSet() );

			Assert.AreEqual( Math.Round( entropy, 5 ), 0.97095 );
		}

		[TestMethod]
		public void TestSplit()
		{
			var set = GetDataSet();

			var split = set.Split( CAN_SURVIVE_WITHOUT_SURFACING, "1" );

			Assert.IsTrue( split.Children.Count == 3 );
		}
		[TestMethod]
		public void TestBestSplit()
		{
			var set = GetDataSet();

			Assert.AreEqual( Decider.SelectBestSplitKey( set ), CAN_SURVIVE_WITHOUT_SURFACING );
		}

		[TestMethod]
		public void CreateTree()
		{
			var tree = GetDataSet().GenerateDecisionTree();
			tree.PrintTreeRecursive();
		}

		[TestMethod]
		public void DecideOnTree()
		{
			var tree = GetDataSet().GenerateDecisionTree();

			var instance = new ID3Node
			{
				Attributes = new List<DecisionTree.Data.Attribute>
										  {
											  new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											  new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										  }
			};

			var output = Tree.ProcessNode( tree, instance );

			Assert.AreEqual( output.Key, IS_FISH );
			Assert.AreEqual( output.Value, "yes" );
		}

		[TestMethod]
		public void DecideOnTreeNoFish()
		{
			var tree = GetDataSet().GenerateDecisionTree();

			var instance = new ID3Node
			{
				Attributes = new List<DecisionTree.Data.Attribute>
										  {
											  new DecisionTree.Data.Attribute("0", CAN_SURVIVE_WITHOUT_SURFACING),
											  new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										  }
			};

			var output = Tree.ProcessNode( tree, instance );

			Assert.AreEqual( output.Key, IS_FISH );
			Assert.AreEqual( output.Value, "no" );
		}

		[TestMethod]
		public void TestLenses()
		{
			var file = @"..\..\..\DataMining\Assets\Lenses\lenses.data";
			var set = new Lenses().Parse( file );

			var tree = set.GenerateDecisionTree();

			tree.PrintTreeRecursive();

			foreach ( var instance in set.Children )
			{
				Assert.AreEqual( Tree.ProcessNode( tree, instance ).Value, instance.Decision.Value );
			}
		}

		[TestMethod]
		public void TestSerialization()
		{
			var data = GetDataSet().GenerateDecisionTree();

			var serialized = data.Serialize();

			var deserializedTree = Extensions.Deserialize<Tree>( serialized );

			Assert.IsTrue( deserializedTree.Branches.Count == data.Branches.Count );
		}

		[TestMethod]
		public void TestCar()
		{
			var file = @"..\..\..\DataMining\Assets\CarEvaluation\car.data";
			var set = new Car().Parse( file );

			var tree = set.GenerateDecisionTree();

			tree.PrintTreeRecursive();

			foreach ( var instance in set.Children )
			{
				Assert.AreEqual( Tree.ProcessNode( tree, instance ).Value, instance.Decision.Value );
			}
		}

		public ID3Tree GetDataSet()
		{

			#region data

			var instance1 = new ID3Node
			{
				Decision = new Decision( "yes", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance2 = new ID3Node
			{
				Decision = new Decision( "yes", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance3 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("0", HAS_FLIPPERS)
										   }
			};

			var instance4 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("0", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance5 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("0", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			#endregion

			return new DecisionTree.Data.ID3Tree
			{
				Children = new List<ID3Node>
										  {
											  instance1,
											  instance2,
											  instance3,
											  instance4,
											  instance5
										  }
			};

		}

        [TestMethod]
        public void SmallDecisionTreeTest()
        {
            DataMiner.Instance.TestingSetSize = 50;
            DataMiner.Instance.GenerateTree();

            for (int i = 0; i < DataMiner.Instance.TestingSetSize; i++)
            {
                DecisionClass ExpertSystemAnswer = ExpertSystem.getDecision(DataMiner.Instance.examples[i].getExample());
                Decision TreeAnswer = Tree.ProcessNode(DataMiner.Instance.decisionTree, DataMiner.Instance.examples[i].getQuestionTreeNode());

                Assert.IsTrue(ExpertSystemAnswer.ToString() == TreeAnswer.Value.ToString());
            }
        }

        [TestMethod]
        public void MediocreDecisionTreeTest()
        {
            DataMiner.Instance.TestingSetSize = 200;
            DataMiner.Instance.GenerateTree();

            for (int i = 0; i < DataMiner.Instance.examples.Count; i++)
            {
                DecisionClass ExpertSystemAnswer = ExpertSystem.getDecision(DataMiner.Instance.examples[i].getExample());
                Decision TreeAnswer = Tree.ProcessNode(DataMiner.Instance.decisionTree, DataMiner.Instance.examples[i].getQuestionTreeNode());

                Assert.IsTrue(ExpertSystemAnswer.ToString() == TreeAnswer.Value.ToString());
            }
        }

        
        [TestMethod]
        public void BigDecisionTreeTest()
        {
            DataMiner.Instance.TestingSetSize = 10000;
            DataMiner.Instance.GenerateTree();
            int counter = 0;

            for (int i = 0; i < DataMiner.Instance.TestingSetSize; i++)
            {
                DecisionClass ExpertSystemAnswer = ExpertSystem.getDecision(DataMiner.Instance.examples[i].getExample());
                Decision TreeAnswer = Tree.ProcessNode(DataMiner.Instance.decisionTree, DataMiner.Instance.examples[i].getQuestionTreeNode());
                
                if(ExpertSystemAnswer.ToString() != TreeAnswer.Value.ToString())
                    counter++;
            }
            Assert.IsTrue(counter == 0);
        }
/*
        [TestMethod]
        public void StressDecisionTreeTest()
        {
            BindingClass.Instance.TestingSetSize = 50000;
            DataMiner.Instance.GenerateTree();

            for (int i = 0; i < BindingClass.Instance.TestingSetSize; i++)
            {
                DecisionClass ExpertSystemAnswer = ExpertSystem.getDecision(DataMiner.Instance.examples[i].getExample());
                Decision TreeAnswer = Tree.ProcessNode(DataMiner.Instance.decisionTree, DataMiner.Instance.examples[i].getQuestionTreeNode());

                Assert.IsTrue(ExpertSystemAnswer.ToString() == TreeAnswer.Value.ToString());
            }
        }
        */
    }
}
