﻿using DataMining.Algorithm;
using DataMining.Config;
using DataMining.DB;
using DataMining.DB.DataGenerators;
using DataMining.DB.ExampleDB;
using DataMining.DecisionTree.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataMining
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public MainWindow()
        {
            DataContext = DataMiner.Instance;
            InitializeComponent();
        }

        private delegate void treeGenerator();

        public void GenerateTreeAsync()
        {
            treeGenerator worker = new treeGenerator(DataMiner.Instance.GenerateTree);
            AsyncCallback completedCallback = new AsyncCallback(MyTaskCompletedCallback);

            AsyncOperation async = AsyncOperationManager.CreateOperation(null);
            worker.BeginInvoke( completedCallback, async);
        }

        private void MyTaskCompletedCallback(IAsyncResult ar)
        {
            // get the original worker delegate and the AsyncOperation instance
            treeGenerator worker =
              (treeGenerator)((AsyncResult)ar).AsyncDelegate;
            AsyncOperation async = (AsyncOperation)ar.AsyncState;

            // finish the asynchronous operation
            worker.EndInvoke(ar);

            // raise the completed event
            AsyncCompletedEventArgs completedArgs = new AsyncCompletedEventArgs(null,
              false, null);
            async.PostOperationCompleted(
              delegate (object e) { OnMyTaskCompleted(); },
              completedArgs);
        }
        

        protected virtual void OnMyTaskCompleted()
        {
            loading.Visibility = Visibility.Hidden;
            Everything.IsEnabled = true;
            ClustererPanel.initialize
                (
                    DataMiner.Instance.gasDecider,
                    DataMiner.Instance.waterDecider,
                    DataMiner.Instance.elecDecider,
                    DataMiner.Instance.yearDecider,
                    DataMiner.Instance.houseDecider
                );
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            loading.Visibility = Visibility.Visible;
            Everything.IsEnabled = false;
            GenerateTreeAsync();
        }
    }
}
