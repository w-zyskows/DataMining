﻿using DataMining.DB.HouseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace DataMining.Config
{
    class IndexToGasVendor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((VendorType)value)
            {
                default: return 0;
                case VendorType.BP: return 0;
                case VendorType.Gazprom: return 1;
                case VendorType.PGNIG: return 2;
                case VendorType.Shell: return 3;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((int)value)
            {
                default: return VendorType.BP;
                case 0: return VendorType.BP;
                case 1: return VendorType.Gazprom;
                case 2: return VendorType.PGNIG;
                case 3: return VendorType.Shell;
            }
        }
    }

    class IndexToElectricityVendor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((VendorType)value)
            {
                default: return 0;
                case VendorType.Enea: return 0;
                case VendorType.Energea: return 1;
                case VendorType.PGE: return 2;
                case VendorType.RWE: return 3;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((int)value)
            {
                default: return VendorType.Enea;
                case 0: return VendorType.Enea;
                case 1: return VendorType.Energea;
                case 2: return VendorType.PGE;
                case 3: return VendorType.RWE;
            }
        }
    }

    class TextToDocConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ICSharpCode.AvalonEdit.Document.TextDocument doc = new ICSharpCode.AvalonEdit.Document.TextDocument();
            doc.Text = (string)value;
            return doc;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class IsNullToVisibility: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Visibility.Hidden;
            else
                return Visibility.Visible;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
