﻿using DataMining.DB;
using DataMining.DB.BillDB;
using DataMining.DB.DataGenerators;
using DataMining.DB.ExampleDB;
using DataMining.DecisionTree.Data;
using DataMining.DecisionTree.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.Config
{
    public class DataMiner : INotifyPropertyChanged
    {
        /**
         *  BINDING boiler-plate
         *  from http://stackoverflow.com/a/1316417
         **/
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        // singleton pattern
        static readonly DataMiner _instance = new DataMiner();
        public static DataMiner Instance { get { return _instance; } }
        private DataMiner()
        {
            gasDecider = new Clusterer();
            waterDecider = new Clusterer();
            elecDecider = new Clusterer();
            houseDecider = new Clusterer();
            yearDecider = new Clusterer();
        }
        public List<DecisionExample> examples = new List<DecisionExample>();
        public List<ID3Node> QueryDataBaseSet(int numberOfElements)
        {
            List<ID3Node> databaseSet = new List<ID3Node>();
            using (var ctx = new DatabaseContext())
            {
                
                while (ctx.Examples.Count() < numberOfElements)
                {
                    TestData.GenerateDatabaseSet(25);
                    int count = ctx.Examples.Count();
                }
               examples = ctx.Examples.Include("GasBills").Include("WaterBills").Include("ElectricityBills").Include("House").Take(numberOfElements).ToList();

                gasDecider.Cluster(examples.Select(b => b.GasBills).SelectMany(list => list).Distinct().Select(f => f.Cost).ToList());
                waterDecider.Cluster(examples.Select(b => b.WaterBills).SelectMany(list => list).Distinct().Select(f => f.Cost).ToList());
                elecDecider.Cluster(examples.Select(b => b.ElectricityBills).SelectMany(list => list).Distinct().Select(f => f.Cost).ToList());
                houseDecider.Cluster(examples.Select(b => b.House).Select(f => f.Size).ToList());
                yearDecider.Cluster(examples.Select(b => b.House).Select(f => (float)f.Age).ToList());
                
                foreach ( DecisionExample DExample in examples )
                    databaseSet.Add(DExample.getTreeNode());
            }
            return databaseSet;
        }



        public void GenerateTree()
        {
            decisionTree = GenerateTreeFromDB(DataMiner.Instance.TestingSetSize);

            decisionTree.s = new StringBuilder();
            if (DataMiner.Instance.TestingSetSize < 5000)
                databaseTreeText = Tree.PrintTree(decisionTree);
            else
                databaseTreeText = "Too big dataset to print!";
            //databaseTreeText = decisionTree.PrintTreeRecursive();
        }
        public Tree GenerateTreeFromDB(int numberOfElements)
        {
            if (testingSet == null || testingSet.Count != numberOfElements)
                testingSet = QueryDataBaseSet(numberOfElements);
            var set = new ID3Tree();
            set.Children = testingSet;
            //set.ApplyClustering();
            return set.GenerateDecisionTree();
            //SaveDatabaseTreeToFile();
        }
        public void GenerateTreeFromFile()
        {
            var path = @".\tree.data";
            var serialized = File.ReadAllText(path);
            //decisionTree = new Tree();
            decisionTree = Extensions.Deserialize<Tree>(serialized);
            decisionTree.s = new StringBuilder();
        }

        private void SaveDatabaseTreeToFile()
        {
            var path = @".\tree.data";
            File.WriteAllText(path, decisionTree.Serialize());
        }

        public Clusterer gasDecider { get; set; }
        public Clusterer waterDecider { get; set; }
        public Clusterer elecDecider { get; set; }
        public Clusterer houseDecider { get; set; }
        public Clusterer yearDecider { get; set; }
        public List<ID3Node> testingSet { get; set; }

        private Tree _decisionTree;
        public Tree decisionTree
        {
            get
            {
                return _decisionTree;
            }
            set
            {
                SetField(ref _decisionTree, value, "decisionTree");
            }
        }

        private string _databaseTreeText = "TREE";
        public string databaseTreeText {
			get
			{
				return _databaseTreeText;
			}
			set
            {
                SetField(ref _databaseTreeText, value, "databaseTreeText");
            }
		}
		public string testingTreeText { get; set; }

        public void inputToDBTree() { }
        public void inputToTestingTree() { }

        private int _testingSetSize = 200;
        public int TestingSetSize
        {
            get { return _testingSetSize; }
            set { SetField(ref _testingSetSize, value, "TestingSetSize"); }
        }

    }
}
