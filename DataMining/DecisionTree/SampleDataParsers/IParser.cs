﻿using DataMining.DecisionTree.Data;

namespace SampleDataParsers
{
    public interface IParser
    {
        ID3Tree Parse(string file);
    }
}
