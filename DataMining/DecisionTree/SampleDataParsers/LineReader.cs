﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DataMining.DecisionTree.Data;

namespace SampleDataParsers
{
    public abstract class LineReader : IParser
    {
        public ID3Tree Parse(string file)
        {
            var set = new ID3Tree();

            set.Children = new List<ID3Node>();

            using (var stream = new StreamReader(file))
            {
                while (!stream.EndOfStream)
                {
                    var line = stream.ReadLine();
                    set.Children.Add(ParseLine(line));
                }
            }

            return set;
        }

        protected abstract ID3Node ParseLine(string line);
    }
}
