﻿using System;
using System.Collections.Generic;
using System.IO;
using DataMining.DecisionTree.Data;

namespace SampleDataParsers.Lenses
{
    public enum PatientAge
    {
        Young = 1,
        PrePresbyopic = 2,
        Presbypic = 3
    }

    public enum SpectacleRx
    {
        Myope = 1,
        Hypermetrope = 2
    }

    public enum Astigmatic
    {
        No = 1,
        Yes = 2
    }

    public enum TearRateProduction
    {
        Reduced = 1,
        Normal = 2
    }

    public enum LenseType
    {
        Hard = 1,
        Soft = 2,
        None = 3
    }

    public class Lenses : LineReader
    {        
        protected override ID3Node ParseLine(string line)
        {
            var splits = line.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var age = Enum.Parse(typeof (PatientAge), splits[1]);
            var rx = Enum.Parse(typeof (SpectacleRx), splits[2]);
            var astigmatic = Enum.Parse(typeof (Astigmatic), splits[3]);
            var tearRate = Enum.Parse(typeof (TearRateProduction), splits[4]);
            var needsContacts = Enum.Parse(typeof (LenseType), splits[5]);

            return new ID3Node
			{
                       Decision = new Decision( needsContacts.ToString(), "contact type"),
                       Attributes = new List<DataMining.DecisionTree.Data.Attribute>
								  {
                                      new DataMining.DecisionTree.Data.Attribute(age.ToString(), "age"),
                                      new DataMining.DecisionTree.Data.Attribute(rx.ToString(), "rx"),
                                      new DataMining.DecisionTree.Data.Attribute(astigmatic.ToString(), "astigmatic"),
                                      new DataMining.DecisionTree.Data.Attribute(tearRate.ToString(), "tearRate")
                                  }
                   };
        }
    }
}
