﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DataMining.DecisionTree.Data;
using SampleDataParsers.Lenses;

namespace SampleDataParsers.Car
{
    public class Car : LineReader
    {
        protected override ID3Node ParseLine(string line)
        {
            var splits = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var buying = splits[0];
            var maintence = splits[1];
            var doors = splits[2];
            var people = splits[3];
            var lugBoot = splits[4];
            var safety = splits[5];

            return new ID3Node
			{
                Decision = new Decision( splits[ 6], "car acceptability"),
                Attributes = new List<DataMining.DecisionTree.Data.Attribute>
								  {
                                      new DataMining.DecisionTree.Data.Attribute(buying, "buying"),
                                      new DataMining.DecisionTree.Data.Attribute(maintence, "maintence"),
                                      new DataMining.DecisionTree.Data.Attribute(doors, "doors"),
                                      new DataMining.DecisionTree.Data.Attribute(people, "people"),
                                      new DataMining.DecisionTree.Data.Attribute(lugBoot, "lugboot"),
                                      new DataMining.DecisionTree.Data.Attribute(safety, "safety")
                                  }
            };
        }
    }
}
