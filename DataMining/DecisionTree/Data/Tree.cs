﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MoreLinq;
using System.Text;
using DataMining.DB.ExampleDB;

namespace DataMining.DecisionTree.Data
{
	/// <summary>
	/// Generic Tree data structure
	/// </summary>
	[DataContract]
	[KnownType( typeof( Tree ) )]
	[KnownType( typeof( Decision ) )]
	[KnownType( typeof( Attribute ) )]
	public class Tree
	{
        public Tree () { s = new StringBuilder(); }

        public StringBuilder s = new StringBuilder();

		/// <summary>
		/// single reference to a leaf decision
		/// </summary>
		[DataMember]
		public Decision Leaf { get; set; }

		/// <summary>
		/// List of subtrees = branches
		/// </summary>
		[DataMember]
		public Dictionary<Attribute, Tree> Branches { get; set; }

		/// <summary>
		/// Recursively print the tree to the console
		/// Useful for debugging purposes
		/// </summary>
		/// <param name="level">tree level used for indentation</param>
		public string PrintTreeRecursive( int level = 0 )
		{

			// custom indentation delegate
			Action indentAction = () => Enumerable.Range( 0, level ).ForEach( i => s.Append( "\t" ) );

			if ( Branches == null )
			{ // no branches, just a decision
				indentAction();
				s.AppendLine( Leaf.ToString() );
			}
			else
			{ // have branches, print them
				foreach ( var feature in Branches )
				{
					indentAction();
                    s.AppendLine( feature.Key.ToString());
					s.AppendLine( feature.Value.PrintTreeRecursive( level + 1 ) );
				}
			}
            return s.ToString();
		}

		/// <summary>
		/// Recursively iterate through the tree structure to find the associated decision leaf
		/// </summary>
		/// <param name="tree">(sub-) tree to process</param>
		/// <param name="node"></param>
		/// <returns></returns>
		public static Decision ProcessNode( Tree tree, ID3Node node )
		{
            if ( tree == null )
            {
                return new Decision( DecisionClass.Uknown.ToString(), Const.DECISION);
            }
			if ( tree.Leaf != null )
			{
				return tree.Leaf;
			}

			return ProcessNode( tree.SubtreeRootedAt( node ), node );
		}

		/// <summary>
		/// Get the Subtree rooted by the attribute
		/// Essentially accessor for the Branches Dictionary
		/// </summary>
		/// <param name="attr"></param>
		/// <returns></returns>
		private Tree GetBranchForAttribute( Attribute attr )
		{
			Tree found;
			if ( Branches.TryGetValue( attr, out found ) )
			{
				return found;
			}
			return null;
		}

		/// <summary>
		/// Get the Subtree rooted at node
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		private Tree SubtreeRootedAt( ID3Node node )
		{
			var tree = node.Attributes.Select( GetBranchForAttribute ).FirstOrDefault( f => f != null );
			return tree;
		}

        public static string PrintTree(Tree tree)
        {
            StringBuilder treeString = new StringBuilder();
            List<Tuple< Attribute,Tree >> firstStack = new List<Tuple<Attribute, Tree>>();
            Tuple<Attribute, Tree> treeTuple = new Tuple<Attribute, Tree>(new Attribute("Root", ""), tree);
            firstStack.Add(treeTuple);

            List<List<Tuple<Attribute, Tree>>> childListStack = new List<List<Tuple<Attribute, Tree>>>();
            childListStack.Add(firstStack);

            while (childListStack.Count > 0)
            {
                List<Tuple<Attribute, Tree>> childStack = childListStack[childListStack.Count - 1];

                if (childStack.Count == 0)
                {
                    childListStack.RemoveAt(childListStack.Count - 1);
                }
                else
                {
                    treeTuple = childStack[0];
                    childStack.RemoveAt(0);

                    string indent = "";
                    for (int i = 0; i < childListStack.Count - 1; i++)
                    {
                        indent += (childListStack[i].Count > 0) ? "|\t" : " \t";
                    }

                    if (treeTuple.Item2.Leaf != null)
                    {
                        treeString.AppendLine(indent + "+- " + "[" + treeTuple.Item1.Key + "]: " + treeTuple.Item1.Value);
                        indent += (childStack.Count > 0) ? "|\t" : " \t";
                        treeString.AppendLine(indent + "-> " + "[" + treeTuple.Item2.Leaf.Key + "]: " + treeTuple.Item2.Leaf.Value);
                        treeString.AppendLine(indent);
                    }
                    else
                    {
                        treeString.AppendLine(indent + "+- " + "[" + treeTuple.Item1.Key + "]: " + treeTuple.Item1.Value);

                        List<Tuple<Attribute, Tree>> tempStack = new List<Tuple<Attribute, Tree>>();
                        foreach (KeyValuePair<Attribute, Tree> branch in treeTuple.Item2.Branches)
                        {
                            tempStack.Add(new Tuple<Attribute, Tree>(branch.Key, branch.Value));
                        }
                        childListStack.Add(tempStack);
                    }
                }
            }

            return treeString.ToString();
        }
    }
}
