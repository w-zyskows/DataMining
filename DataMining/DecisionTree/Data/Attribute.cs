﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DataMining.DecisionTree.Data
{
	/// <summary>
	/// Arbitrary key value pair
	/// </summary>
	[DataContract]
	public class Attribute
	{
		/// <summary>
		/// private Equals operator
		/// </summary>
		/// <param name="other">parameter to check</param>
		/// <returns>boolean true if this.Equals(other)</returns>
		protected bool Equals( Attribute other )
		{
			return string.Equals( Value, other.Value ) && string.Equals( Key, other.Key );
		}

        /// <summary>
        /// Public Equals operator
        /// Casts to attribute
        /// </summary>
        /// <param name="obj">parameter to check</param>
        /// <returns>boolean true if this.Equals( other )</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Attribute)obj);
        }

        /// <summary>
		/// Public Gretar Than operator
		/// Casts to attribute
		/// </summary>
		/// <param name="obj">parameter to check</param>
		/// <returns>boolean true if this.GreaterThan( other )</returns>
		public bool GreaterThan(object obj)
        {
            Decimal valueThis;
            Decimal valueOther;
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return false;
            if (!Decimal.TryParse(this.ToString(), out valueThis)) return false;
            if (!Decimal.TryParse(obj.ToString(), out valueOther)) return false;

            if (valueThis > valueOther) return true;
            else return false;
        }

        /// <summary>
		/// Check if an attribute value is parsable
		///					 == is a decimal number
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
        public static bool checkIfDecimal(object obj)
        {
            Decimal dummy;
            return Decimal.TryParse(obj.ToString(), out dummy);
        }

        /// <summary>
        /// Hash code override
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{
			unchecked
			{
				return ( ( Value != null ? Value.GetHashCode() : 0 ) * 397 ) ^ ( Key != null ? Key.GetHashCode() : 0 );
			}
		}

		/// <summary>
		/// Data Key
		/// </summary>
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// Data Value
		/// </summary>
		[DataMember]
		public string Value { get; set; }

		
		/// <summary>
		/// Public constructor
		/// </summary>
		/// <param name="value"></param>
		/// <param name="key"></param>
		public Attribute( string value, string key )
		{
			Value = value;
			Key = key;
		}


		/// <summary>
		/// ToString override
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return String.Format( "[{0}]: {1}", Key, Value );
		}
	}
}
