﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining.DecisionTree.Data
{
	public class ID3Node
	{
		/// <summary>
		/// List of attributes
		/// </summary>
		public List<Attribute> Attributes { get; set; }


		/// <summary>
		/// If this node is a leaf node, this is the reference to the decision
		/// </summary>
		public Decision Decision { get; set; }

		/// <summary>
		/// Split this node along a key/value pair
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public ID3Node Split( string key, string value )
		{
			// choose the attribute to split by
			var splitAttribute = new Attribute( value, key );
			// split the attribute set s.t. the split attribute is not included
			var attributeSplit = Attributes.Where( f => !f.Equals( splitAttribute ) ).ToList();

			if ( attributeSplit.Count == Attributes.Count )
			{	// split didn't change attribute set
				attributeSplit = new List<Attribute>();
			}

			return new ID3Node
			{
				Decision = Decision,
				Attributes = attributeSplit
			};
		}

		/// <summary>
		/// ToString override
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			var s = Attributes.Aggregate( String.Empty, ( acc, item ) => acc + item.Value + ", " );

			s += Decision.Value;

			return s;
		}
	}
}
