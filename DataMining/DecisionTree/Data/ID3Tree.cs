﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoreLinq;

namespace DataMining.DecisionTree.Data
{
	/// <summary>
	/// Iterative Dichotomiser 3 algorithm
	/// Tree building class
	/// </summary>
	public class ID3Tree
	{

		/// <summary>
		/// List of decision nodes
		/// </summary>
		public List<ID3Node> Children { get; set; }

		/// <summary>
		/// Accessor for number of attributes
		/// </summary>
		public int NumberOfAttributes
		{
			get
			{
				var first = Children.FirstOrDefault();
				if ( first == null ) return 0;
				return first.Attributes.Count;
			}
		}

		/// <summary>
		/// Accessor for number of nodes
		/// </summary>
		public int NumberOfChildren
		{
			get { return Children.Count(); }
		}

		/// <summary>
		/// Checks if all Decision Nodes are in the same class
		/// </summary>
		public Boolean DecisionNodesHaveOneDistinctValue
		{
			get
			{
				return Children.Select( i => i.Decision ).DistinctBy( i => i.Value ).Count() == 1;
			}
		}

		/// <summary>
		/// Helper function to split the tree by an Attribute
		/// </summary>
		/// <param name="attr">Attribute to split by</param>
		/// <returns>Split ID3Tree</returns>
		public ID3Tree Split( Attribute attr )
		{
			return Split( attr.Key, attr.Value );
		}

		/// <summary>
		/// Split tree by key/value pair
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public ID3Tree Split( string key, string value )
		{
			return new ID3Tree
			{
				// split the decision nodes by key/value pair
				// keep only the ones which have attributes
				Children = Children.Select( i => i.Split( key, value ) )
											.Where( i => i.Attributes.Any() )
											.ToList()
			};
		}


		/// <summary>
		/// ID3: 
		/// Entry point for the algorithm 
		/// After adding the examples with their attributes to the tree structure,
		/// this function is responsible for splitting the tree by the key with biggest info gain
		/// </summary>
		/// <returns></returns>
		public Tree GenerateDecisionTree()
		{
            // if only one distinct value or only one attribute (no more decisions, return the leaf)
            if ( DecisionNodesHaveOneDistinctValue || Children.All( f => f.Attributes.Count() == 1 ) )
			{
				return LeafTreeForRemainingAttributes();
			}

            var best = Decider.SelectBestSplitKey( this );

			return SplitByKey( best );
		}

		/// <summary>
		/// Split the Tree on each unique attribute which has the given key
		/// </summary>
		/// <param name="key"></param>
		/// <returns>Tree</returns>
		private Tree SplitByKey( string key )
		{
			if ( key == null ) // sanity check
			{
				return null;
			}

            // split the set on each unique attribute which has the given key
            // list of splits				 // get list of unique attributes with this key	
            var splits = ( from attribute in UniqueAttributes().Where( a => a.Key == key ) 
						   select new {
							   splitAttribute = attribute,
							   set = Split( attribute ) // create new set for this split
						   } ).ToList();

			var branches = new Dictionary<Attribute, Tree>();

			// for each split, either recursively create a new tree
			// or split the final attribute outputs into leaf trees
			foreach ( var item in splits )
			{
				branches[ item.splitAttribute ] = item.set.GenerateDecisionTree();
			}

			return new Tree
			{
				Branches = branches
			};
		}


		/// <summary>
		/// Generate the subtree for the remaining attributes
		/// </summary>
		/// <returns></returns>
		private Tree LeafTreeForRemainingAttributes()
		{
			// if only one value
			if ( DecisionNodesHaveOneDistinctValue )
			{
				return LeafTreeByDistinctDecisionValue();
			}

			// if only one attribute
			if ( Children.All( f => f.Attributes.Count() == 1 ) )
			{
				return LeafTreeForEachAttribute();
			}

			return null;
		}


		/// <summary>
		/// Generate leaf-subtrees for every attribute
		/// Skip branches with duplicate attributes
		/// </summary>
		/// <returns>Subtree with only leafs</returns>
		private Tree LeafTreeForEachAttribute()
		{
			var branches = new Dictionary<Attribute, Tree>();

			foreach ( var child in Children )
			{
				foreach ( var attribute in child.Attributes )
				{
					if ( branches.Any( k => k.Key.Value == attribute.Value ) )
					{
						continue;
					}

					branches[ attribute ] = new Tree
					{
						Leaf = child.Decision
					};
				}
			}

			return new Tree
			{
				Branches = branches
			};
		}

		/// <summary>
		/// Generate leaf-subtrees for distinct decision values
		/// return this in branches or just as a single leaf
		/// </summary>
		/// <returns></returns>
		private Tree LeafTreeByDistinctDecisionValue()
		{
			// get children with distinct decision
			// convert to new Dictionary < Attribute, Leaf Tree > (= basically new set of branches with leaf trees)
			var leafBranches = Children .DistinctBy( i => i.Decision.Value )
										.ToDictionary(
											i => i.Attributes.First(), 
											j => new Tree
											{
												Leaf = j.Decision
											} 
										);

			if ( leafBranches.Count() > 1 ) // if we have more than one such leafbranch
			{
				return new Tree // return tree with these branches
				{
					Branches = leafBranches
				};
			}
			
			return new Tree // return tree with first leaf branch
			{
				Leaf = leafBranches.First().Value.Leaf
			};
		}

		/// <summary>
		/// Gets the list of unique key/value pairs
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Attribute> UniqueAttributes()
		{
			//var b = Children.SelectMany( f => f.Attributes );
            return Children.SelectMany( f => f.Attributes ).DistinctBy( f => f.Key + f.Value ).ToList();
		} 
    }
}


