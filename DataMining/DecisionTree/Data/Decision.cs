﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DataMining.DecisionTree.Data
{
	/// <summary>
	/// Generic Leaf Node
	/// Logical distinction between attributes and decisions
	/// </summary>
	[DataContract]
	public class Decision : Attribute
	{
		public Decision( string value, string @class )
			: base( value, @class )
		{

		}
	}
}
