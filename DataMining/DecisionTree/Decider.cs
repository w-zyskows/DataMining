﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMining.DecisionTree.Data;

namespace DataMining.DecisionTree
{
	public static class Decider
    {
        public static int THRESHOLD = 2;
        public static int CLUSTER_NUMBER = 2;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="tree"></param>
		/// <returns></returns>
        public static string SelectBestSplitKey( ID3Tree tree )
		{
			// estimate the base entropy of the data set
			var baseEntropy = Entropy( tree );
			var bestInfoGain = 0.0;

			var uniqueAttributesGroupedByKey = tree.UniqueAttributes().GroupBy( i => i.Key ).ToList();

			// initialize first best split
			string bestSplitKey = uniqueAttributesGroupedByKey.First().Key;

			// calculate the total entropy based on splitting by this key. 
			// The total entropy is the sum of the entropy of each branch 
			//						  that would be created by this split 
			foreach ( var key in uniqueAttributesGroupedByKey )
			{
				var newEntropy = EntropyForSplitBranches( tree, key.ToList() );
				var infoGain = baseEntropy - newEntropy;
				if ( infoGain > bestInfoGain )
				{
					bestInfoGain = infoGain;
					bestSplitKey = key.Key;
				}
			}
			return bestSplitKey;
		}

		/// <summary>
		/// Calculate the entropy for the entire tree
		/// </summary>
		/// <param name="tree"></param>
		/// <returns></returns>
		public static double Entropy( Data.ID3Tree tree )
		{
			var total = tree.Children.Count();
			var nodes = tree.Children.Select( i => i.Decision ).GroupBy( f => f.Value ).ToList();
			var entropy = 0.0;

			foreach ( var node in nodes )
			{ // from definition of entropy
				var probability = (float) node.Count() / total;
				entropy -= probability * Math.Log( probability, 2 );
			}

			return entropy;
		}

		/// <summary>
		/// Calculate the entropy for all possible splits
		/// </summary>
		/// <param name="tree"></param>
		/// <param name="allPossibleKeyValuePairs"></param>
		/// <returns></returns>
		private static double EntropyForSplitBranches( Data.ID3Tree tree, IEnumerable<Data.Attribute> allPossibleKeyValuePairs )
		{
    		// calculate the SUM of
			//						relative probability * entropy
			// for each 
			//			subtree which was generated 
			//			by splitting the new branch set
			return ( 
						 from possibleValue in allPossibleKeyValuePairs
							select tree.Split( possibleValue ) into subtree
						 let probability = (float) subtree.NumberOfChildren / tree.NumberOfChildren
							select probability * Entropy( subtree ) 
					).Sum();
		}
    }

}
