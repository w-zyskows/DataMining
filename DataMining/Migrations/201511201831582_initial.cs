namespace DataMining.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "BillType", c => c.Int(nullable: false));
            DropColumn("dbo.Bills", "Vendor");
            DropColumn("dbo.Bills", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bills", "Date", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Bills", "Vendor", c => c.Int(nullable: false));
            DropColumn("dbo.Bills", "BillType");
        }
    }
}
