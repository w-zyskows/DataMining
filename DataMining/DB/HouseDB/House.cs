﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.HouseDB
{
    public class House
    {
        public int HouseId { get; set; }
        public float Size { get; set; }
        public int Age { get; set; }
        public int InhabitantsNumber { get; set; }
        public HouseType HouseType { get; set; }
        public HeatingType HeatingType { get; set; }
        public MaterialType MaterialType { get; set; }
    }
}
