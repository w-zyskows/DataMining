﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.HouseDB
{
    public enum HouseType
    {
        Detached            = 0,
        SemiDetached        = 1,
        Flat                = 2,
        ModernStyleFlat     = 3,
        Tenement            = 4,
        LPSHouse            = 5
    }
}
