﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.HouseDB
{
    public enum  VendorType
    {
        BP      = 1,
        Gazprom = 2,
        PGNIG   = 3,
        Shell   = 4,
        Enea    = 5,
        Energea = 6,
        PGE     = 7,
        RWE     = 8,
        Tauron  = 9,
        Water   = 10
    }
}
