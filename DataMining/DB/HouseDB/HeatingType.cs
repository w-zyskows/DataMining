﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.HouseDB
{
    public enum HeatingType
    {
        Wood        = 0,
        Charcoal    = 1,
        LiquidFuel  = 2,
        Gas         = 3,
        Electrical  = 4
    }
}
