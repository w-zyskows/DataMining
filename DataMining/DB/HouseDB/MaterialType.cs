﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.HouseDB
{
    public enum MaterialType
    {
        Wood                = 0,
        Brick               = 1,
        AirBrick            = 2,
        LPSMaterial         = 3,
        Styrofoam           = 4,
        Siding              = 5
    }
}
