﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.ExampleDB
{
    public enum DecisionClass
    {
        ChangeGasVendor         = 0,
        ChangeWaterVendor       = 1,
        ChangeElectricityVendor = 2,
        ChangeHeatingType       = 3,
        ChangeHouseMaterial     = 4,
        BuyEnergySavingBulbs    = 5,
        MoveOut                 = 6,
        LeaveAsIs               = 7,
        Uknown                  = 8
    }
}
