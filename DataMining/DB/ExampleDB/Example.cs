﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMining.DB.BillDB;
using DataMining.DB.HouseDB;

namespace DataMining.DB.ExampleDB
{
    public class Example
    {
        public House House { get; set; }
        public ICollection<Bill> GasBills { get; set; }
        public ICollection<Bill> WaterBills { get; set; }
        public ICollection<Bill> ElectricityBills { get; set; }
        public VendorType GasVendor { get; set; }
        public VendorType ElectricityVendor { get; set; }
        public VendorType WaterVendor { get; set; }

        public Example(House house, ICollection<Bill> gasBills, ICollection<Bill> waterBills, ICollection<Bill> electricityBills,
            VendorType gasVendor, VendorType electricityVendor, VendorType waterVendor)
        {
            House = house;
            GasBills = gasBills;
            WaterBills = waterBills;
            ElectricityBills = electricityBills;
            GasVendor = gasVendor;
            ElectricityVendor = electricityVendor;
            WaterVendor = waterVendor;
        }

        
        public DecisionExample getDecisionExample(DecisionClass decision)
        {
            return new DecisionExample()
            {
                House = this.House,
                GasBills = this.GasBills,
                WaterBills = this.WaterBills,
                ElectricityBills = this.ElectricityBills,
                GasVendor = this.GasVendor,
                ElectricityVendor = this.ElectricityVendor,
                WaterVendor = this.WaterVendor,
                Decision = decision
            };
        }
        
    }
}
