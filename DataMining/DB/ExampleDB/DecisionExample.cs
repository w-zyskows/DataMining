﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMining;
using DataMining.DB.BillDB;
using DataMining.DB.HouseDB;
using DataMining.DecisionTree.Data;
using DataMining.Config;

namespace DataMining.DB.ExampleDB
{

	public class DecisionExample
    {

		public int DecisionExampleId { get; set; }
        public House House { get; set; }
        public ICollection<Bill> GasBills { get; set; }
        public ICollection<Bill> WaterBills { get; set; }
        public ICollection<Bill> ElectricityBills { get; set; }
        public VendorType GasVendor { get; set; }
        public VendorType ElectricityVendor { get; set; }
        public VendorType WaterVendor { get; set; }
        public DecisionClass Decision { get; set; }

        public ID3Node getTreeNode()
        {
			// prepare list of attributes
			List<DecisionTree.Data.Attribute> attributes = new List<DecisionTree.Data.Attribute>();

			float billCost = 0;
            GasBills.ToList().ForEach(b => billCost += b.Cost);
			float AverageGasBillCost = billCost / GasBills.Count;
			attributes.Add( new DecisionTree.Data.Attribute( DataMiner.Instance.gasDecider.getCluster( AverageGasBillCost ).ToString(), Const.AVG_GAS_COST ) );

			billCost = 0;
            WaterBills.ToList().ForEach(b => billCost += b.Cost);
			float AverageWaterBillCost = billCost / WaterBills.Count;
			attributes.Add( new DecisionTree.Data.Attribute(DataMiner.Instance.waterDecider.getCluster(AverageWaterBillCost).ToString(), Const.AVG_WATER_COST ) );

			billCost = 0;
            ElectricityBills.ToList().ForEach(b => billCost += b.Cost);
			float AverageElectricityBillCost = billCost / ElectricityBills.Count;
			attributes.Add( new DecisionTree.Data.Attribute(DataMiner.Instance.elecDecider.getCluster(AverageElectricityBillCost).ToString(), Const.AVG_ELECTRICITY_COST ) );

			attributes.Add( new DecisionTree.Data.Attribute( DataMiner.Instance.yearDecider.getCluster(House.Age).ToString(), Const.HOUSE_AGE ) );
			attributes.Add( new DecisionTree.Data.Attribute( ElectricityVendor.ToString(), Const.ELECTRICITY_VENDOR ) );
			attributes.Add( new DecisionTree.Data.Attribute( GasVendor.ToString(), Const.GAS_VENDOR ) );
			attributes.Add( new DecisionTree.Data.Attribute( WaterVendor.ToString(), Const.WATER_VENDOR ) );
			attributes.Add( new DecisionTree.Data.Attribute( DataMiner.Instance.houseDecider.getCluster(House.Size).ToString(), Const.HOUSE_SIZE ) );
			attributes.Add( new DecisionTree.Data.Attribute( House.MaterialType.ToString() , Const.HOUSE_MATERIAL_TYPE ) );
			attributes.Add( new DecisionTree.Data.Attribute( House.InhabitantsNumber.ToString(), Const.HOUSE_INHABITANT_NUMBER ) );
			attributes.Add( new DecisionTree.Data.Attribute( House.HeatingType.ToString(), Const.HOUSE_HEATING_TYPE ) );

			var exampleDecision = new DecisionTree.Data.Decision( this.Decision.ToString(), Const.DECISION );

			return new ID3Node
			{
				Decision = exampleDecision,
				Attributes = attributes
			};
        }

        public ID3Node getQuestionTreeNode()
        {
            // prepare list of attributes
            List<DecisionTree.Data.Attribute> attributes = new List<DecisionTree.Data.Attribute>();

            float billCost = 0;
            GasBills.ToList().ForEach(b => billCost += b.Cost);
            float AverageGasBillCost = billCost / GasBills.Count;
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.gasDecider.getCluster(AverageGasBillCost).ToString(), Const.AVG_GAS_COST));

            billCost = 0;
            WaterBills.ToList().ForEach(b => billCost += b.Cost);
            float AverageWaterBillCost = billCost / WaterBills.Count;
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.waterDecider.getCluster(AverageWaterBillCost).ToString(), Const.AVG_WATER_COST));

            billCost = 0;
            ElectricityBills.ToList().ForEach(b => billCost += b.Cost);
            float AverageElectricityBillCost = billCost / ElectricityBills.Count;
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.elecDecider.getCluster(AverageElectricityBillCost).ToString(), Const.AVG_ELECTRICITY_COST));

            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.yearDecider.getCluster(House.Age).ToString(), Const.HOUSE_AGE));
            attributes.Add(new DecisionTree.Data.Attribute(ElectricityVendor.ToString(), Const.ELECTRICITY_VENDOR));
            attributes.Add(new DecisionTree.Data.Attribute(GasVendor.ToString(), Const.GAS_VENDOR));
            attributes.Add(new DecisionTree.Data.Attribute(WaterVendor.ToString(), Const.WATER_VENDOR));
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.houseDecider.getCluster(House.Size).ToString(), Const.HOUSE_SIZE));
            attributes.Add(new DecisionTree.Data.Attribute(House.MaterialType.ToString(), Const.HOUSE_MATERIAL_TYPE));
            attributes.Add(new DecisionTree.Data.Attribute(House.InhabitantsNumber.ToString(), Const.HOUSE_INHABITANT_NUMBER));
            attributes.Add(new DecisionTree.Data.Attribute(House.HeatingType.ToString(), Const.HOUSE_HEATING_TYPE));
            
            return new ID3Node
            {
                Attributes = attributes
            };
        }

        public Example getExample()
        {
            return new Example
            (
                House,
                GasBills,
                WaterBills,
                ElectricityBills,
                GasVendor,
                ElectricityVendor,
                WaterVendor
            );
        }
    }
}
