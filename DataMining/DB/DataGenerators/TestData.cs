﻿using DataMining.DB.BillDB;
using DataMining.DB.ExampleDB;
using DataMining.DB.HouseDB;
using DataMining.DecisionTree.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.DataGenerators
{
    public class TestData
    {

        private const int minBillNumber = 1;
        private const int maxBillNumber = 12;

        private const int _RowsToGenerateDB = 100;
        static public void GenerateDatabaseSet(int rowsToGenerate = _RowsToGenerateDB)
        {
            using (var ctx = new DatabaseContext())
            {
                for (int i = 0; i < rowsToGenerate; i++)
                {
                    House house = HouseGenerator.createRandomHouse();
                    DecisionExample DExample = generateFilledDecisionExample(house);
                    ctx.Houses.Add(house);
                    ctx.Examples.Add(DExample);
                }
                ctx.SaveChanges();
            }
        }


        private static void removeDExample(DecisionExample DExample)
        {

        }

        private static ICollection<Bill> generateBillList(House house, BillType billType, int billCount)
        {
            ICollection<Bill> bills = new List<Bill>();
            for (int i = 0; i < billCount; i++)
            {
                bills.Add(BillGenerator.generateBill(house, billType));
            }

            using (var ctx = new DatabaseContext())
            {
                for (int i = 0; i < billCount; i++)
                {
                    ctx.Bills.Add(bills.ElementAt(i));
                }
                ctx.SaveChanges();
            }
            return bills;
        }

        private static DecisionExample generateFilledDecisionExample(House house)
        {
            int billCount = RG.GetRandomInt(minBillNumber, maxBillNumber);
            ICollection<Bill> gasBills = generateBillList(house, BillType.Gas, billCount);
            ICollection<Bill> waterBills = generateBillList(house, BillType.Water, billCount);
            ICollection<Bill> electricityBills = generateBillList(house, BillType.Electricity, billCount);

            return DEGenerator.generateDecisionExample(house, gasBills, waterBills, electricityBills);
        }
    }
}
