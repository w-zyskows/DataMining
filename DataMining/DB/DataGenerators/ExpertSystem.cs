﻿using DataMining.DB.BillDB;
using DataMining.DB.ExampleDB;
using DataMining.DB.HouseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.DataGenerators
{
    public class ExpertSystem
    {
       public static DecisionClass getDecision(Example example)
        {
            if (checkChangeGasVendor(example))
                return DecisionClass.ChangeGasVendor;
            if (checkBuyLightbulbs(example))
                return DecisionClass.BuyEnergySavingBulbs;
            if (checkChangeElectricityVendor(example))
                return DecisionClass.ChangeElectricityVendor;
            if (checkChangeHeatingType(example))
                return DecisionClass.ChangeHeatingType;
            if (checkChangeHouseMaterial(example))
                return DecisionClass.ChangeHouseMaterial;
            if (checkMoveOut(example))
                return DecisionClass.MoveOut;
            return DecisionClass.LeaveAsIs;
        }

        private static bool checkChangeGasVendor(Example example)
        {
            costLevel cost = getCostLevelPerPerson(example, BillType.Gas);
            if (
                cost >= costLevel.High ||
                (
                    cost >= costLevel.VeryHigh &&
                    example.House.HeatingType == HeatingType.Gas)
                )
                return true;
            else
                return false;
        }

        private static bool checkBuyLightbulbs(Example example)
        {
            costLevel cost = getCostLevelPerPerson(example, BillType.Electricity);
            if (cost >= costLevel.High )
                return true;
            else
                return false;
        }

        private static bool checkChangeElectricityVendor(Example example)
        {
            costLevel cost = getCostLevelPerPerson(example, BillType.Electricity);
            if (
                cost >= costLevel.High ||
                (
                    cost >= costLevel.VeryHigh &&
                    example.House.HeatingType == HeatingType.Electrical)
                )
                return true;
            else
                return false;
        }

        private static bool checkChangeHeatingType(Example example)
        {
            if (example.House.HeatingType < HeatingType.Gas)
                return true;
            else
                return false;
        }

        private static bool checkChangeHouseMaterial(Example example)
        {
            if (example.House.MaterialType <= MaterialType.LPSMaterial)
                return true;
            else
                return false;
        }

        private static bool checkMoveOut(Example example)
        {
            costLevel costWater = getCostLevelPerPerson(example, BillType.Water);

            if (example.House.HouseType > HouseType.ModernStyleFlat ||
                costWater >= costLevel.High)
                return true;
            else
                return false;
        }

        private static costLevel getCostLevelPerPerson(Example example, BillType billType)
        {
            return getCostLevel(getAverageBillCostPerPerson(example, billType));
        }

        private static float getAverageBillCostPerPerson(Example example, BillType billType)
        {
            ICollection<Bill> bills;
            if (billType == BillType.Electricity)
                bills = example.ElectricityBills;
            else if (billType == BillType.Gas)
                bills = example.GasBills;
            else
                bills = example.WaterBills;

            float averageBilLCost = 0;
            foreach(Bill bill in bills)
            {
                averageBilLCost += bill.Cost;
            }

            return (averageBilLCost/bills.Count)/example.House.InhabitantsNumber;
        }

        private static costLevel getCostLevel(float cost)
        {
            if (cost < (float)costLevel.VeryLow)
                return costLevel.VeryLow;
            if (cost < (float)costLevel.Low)
                return costLevel.Low;
            if (cost < (float)costLevel.High)
                return costLevel.High;
            return costLevel.VeryHigh;
        }

        private enum costLevel
        {
            VeryLow = 200,
            Low = 600,
            High = 900,
            VeryHigh
        }
    }
}
