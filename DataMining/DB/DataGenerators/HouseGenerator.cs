﻿using DataMining.DB.HouseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.DataGenerators
{
    public class HouseGenerator
    {
        static int oldestBuilding = 70;
        static int newestBuilding = 1;

        static float minimalHouseSize = 12;
        static float maximalHouseSize = 200;

        static int minimalInhabitantsNumber = 1;
        static int maximalInhabitantsNumber = 8;

        static int minHeatingType = 0;
        static int maxHeatingType = 4;

        static int minMaterialType = 0;
        static int maxMaterialType = 5;

        static int minHouseType = 0;
        static int maxHouseType = 5;

        public static House createRandomHouse()
        {
            float _Size = RG.GetRandomFloat(minimalHouseSize, maximalHouseSize);
            int _Age = RG.GetRandomInt(newestBuilding, oldestBuilding);
            int _InhabitantsNumber = RG.GetRandomInt(minimalInhabitantsNumber, maximalInhabitantsNumber);
            HouseType _HouseType = (HouseType)RG.GetRandomInt(minHouseType, maxHouseType);
            HeatingType _HeatingType = (HeatingType)RG.GetRandomInt(minHeatingType, maxHeatingType);
            MaterialType _MaterialType = (MaterialType)RG.GetRandomInt(minMaterialType, maxMaterialType);

            House house = new House()
            {
                Size = _Size,
                Age = _Age,
                HeatingType = _HeatingType,
                HouseType = _HouseType,
                InhabitantsNumber = _InhabitantsNumber,
                MaterialType = _MaterialType
            };

            return house;
        }
    }
}
