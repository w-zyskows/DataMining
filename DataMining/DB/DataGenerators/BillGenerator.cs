﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMining.DB.BillDB;
using DataMining.DB.HouseDB;

namespace DataMining.DB.DataGenerators
{
    public class BillGenerator
    {
        static float minCostPerPerson = 100;
        static float maxCostPerPerson = 1000;

        public static Bill generateBill(House house, BillType billType)
        {
            float _Cost = 0;

            for (int i = 0; i < house.InhabitantsNumber; i++)
                _Cost += RG.GetRandomFloat(minCostPerPerson, maxCostPerPerson);

            Bill bill = new Bill()
            {
                Cost = _Cost,
                House = house,
                BillType = billType
            };

            return bill;
        }
    }
}
