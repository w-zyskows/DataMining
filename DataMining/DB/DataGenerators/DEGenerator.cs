﻿using DataMining.DB.BillDB;
using DataMining.DB.ExampleDB;
using DataMining.DB.HouseDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.DataGenerators
{
    public class DEGenerator
    {
        static int fromGasVendor = 1;
        static int toGasVendor = 4;

        static int fromElectricityVendor = 5;
        static int toElectricityVendor = 9;

        static int fromWaterVendor = 10;
        static int toWaterVendor = 10;

        public static Example generateExample(House house, ICollection<Bill> gasBills, ICollection<Bill> waterBills, ICollection<Bill> electricityBills)
        {
            VendorType gasVendor = (VendorType)RG.GetRandomInt(fromGasVendor, toGasVendor);
            VendorType electricityVendor = (VendorType)RG.GetRandomInt(fromElectricityVendor, toElectricityVendor);
            VendorType waterVendor = (VendorType)RG.GetRandomInt(fromWaterVendor, toWaterVendor);

            return new Example(house, gasBills, waterBills, electricityBills, gasVendor, electricityVendor, waterVendor);
        }

        public static DecisionExample generateDecisionExample(House house, ICollection<Bill> gasBills, ICollection<Bill> waterBills, ICollection<Bill> electricityBills)
        {
            Example example = generateExample(house, gasBills, waterBills, electricityBills);
            return example.getDecisionExample(ExpertSystem.getDecision(example)); 
        }
    }
}
