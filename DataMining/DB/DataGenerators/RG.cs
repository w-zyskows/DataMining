﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.DataGenerators
{
    public class RG
    {
        static Random rnd = new Random();

        static public float GetRandomFloat(float minimum, float maximum)
        {
            return Convert.ToSingle(rnd.NextDouble() * (maximum - minimum) + minimum);
        }
        static public int GetRandomInt(int minimum, int maximum)
        {
            return rnd.Next(minimum, maximum + 1);
        }

        static public DateTime RandomDay(DateTime start, DateTime end)
        {
            int range = (end - start).Days;
            return start.AddDays(rnd.Next(range));
        }
    }
}
