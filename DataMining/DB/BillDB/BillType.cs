﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.BillDB
{
    public enum BillType
    {
        Gas = 1,
        Electricity = 2,
        Water = 3
    }
}
