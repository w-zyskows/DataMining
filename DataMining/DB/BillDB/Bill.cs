﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMining.DB.HouseDB;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataMining.DB.BillDB
{
    public class Bill
    {
        public int BillId { get; set; }
        public BillType BillType { get; set; }
        public House House { get; set; }
        public float Cost { get; set; }
    }
}
