﻿using DataMining.DB.ExampleDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.DB.BillDB
{
    public enum CostType
    {
        vlow = 0,
        low = 1,
        average = 2,
        high = 3,
        vhigh = 4
    }

    public class Clusterer
    {
        private Dictionary<float, CostType> lookupTable = new Dictionary<float, CostType>();
        private int clusterNumber = System.Enum.GetValues(typeof(CostType)).Length;

        /// <summary>
        /// Cluster the attribute list so that each subset has ca. the same number of elements
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public void Cluster(ICollection<float> list)
        {
            lookupTable.Clear();
            // order the list by applying the generic C# operator
            float counter = list.Count() / clusterNumber;
            float rest = list.Count() % clusterNumber;

            list = list.OrderBy( f => f ).ToList();

            // cluster the values (almost) evenly
            for (int x = 0; x < clusterNumber; x++)
            {
                float mean = 0;
                int count = 0;
                for (int i = 0; i < counter; i++)
                {
                    mean += list.ElementAt(x * (int)counter + i);
                    count++;
                }
                mean = mean / count;
                lookupTable.Add(mean, (CostType)x);
            }
        }

        public CostType getCluster(float cost)
        {
            float distance = Math.Abs(lookupTable.Keys.ElementAt(0) - cost);
            int key = 0;
            for (int i = 1; i < clusterNumber; i++)
            {
                float tempDist = Math.Abs(lookupTable.Keys.ElementAt(i) - cost);
                if (distance > tempDist)
                {
                    distance = tempDist;
                    key = i;
                }
            }
            return lookupTable.Values.ElementAt(key);
        }

        public int getCostType(int x)
        {
            return (int)lookupTable.ElementAt(x).Key;
        }
    }
}
