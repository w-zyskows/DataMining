﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMining
{
	public class Const
	{
		public const string HOUSE_AGE = "House.BuildingAge";
		public const string AVG_GAS_COST = "AverageGasBillCost";
		public const string AVG_WATER_COST = "AverageWaterBillCost";
		public const string AVG_ELECTRICITY_COST = "AverageElectricityBillCost";
		public const string ELECTRICITY_VENDOR = "ElectricityVendor";
		public const string GAS_VENDOR = "GasVendor";
		public const string WATER_VENDOR = "WaterVendor";
		public const string HOUSE_SIZE = "House.Size";
		public const string HOUSE_MATERIAL_TYPE = "House.MaterialType";
		public const string HOUSE_INHABITANT_NUMBER = "House.InhabitantsNumber";
		public const string HOUSE_HEATING_TYPE = "House.HeatingType";
		public const string DECISION = "Decision";
	}
}
