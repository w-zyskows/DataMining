﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataMining.Config;
using DataMining.DecisionTree.Data;
using SampleDataParsers.Car;

namespace DataMining.XAML
{
	/// <summary>
	/// Interaction logic for TestButtons.xaml
	/// </summary>
	public partial class TestButtons : UserControl
	{
		public TestButtons()
		{
            this.DataContext = DataMiner.Instance;
			InitializeComponent();
		}


        /*
		const string CAN_SURVIVE_WITHOUT_SURFACING = "can survive without surfacing";
		const string HAS_FLIPPERS = "has flippers";
		const string IS_FISH = "is fish";

        private ID3Tree GetDataSet()
		{

			#region data

			var instance1 = new ID3Node
			{
				Decision = new Decision( "yes", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance2 = new ID3Node
			{
				Decision = new Decision( "yes", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance3 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("1", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("0", HAS_FLIPPERS)
										   }
			};

			var instance4 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("0", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			var instance5 = new ID3Node
			{
				Decision = new Decision( "no", IS_FISH ),
				Attributes = new List<DecisionTree.Data.Attribute>
										   {
											   new DecisionTree.Data.Attribute("0", CAN_SURVIVE_WITHOUT_SURFACING),
											   new DecisionTree.Data.Attribute("1", HAS_FLIPPERS)
										   }
			};

			#endregion

			return new DecisionTree.Data.ID3Tree
			{
				Children = new List<ID3Node>
										  {
											  instance1,
											  instance2,
											  instance3,
											  instance4,
											  instance5
										  }
			};

		}

		private void Button_Click( object sender, RoutedEventArgs e )
		{
			var file = @"..\..\..\DataMining\Assets\CarEvaluation\car.data";
			var set = new Car().Parse( file );

			var tree = set.GenerateDecisionTree();
			DataMiner.Instance.databaseTreeText = tree.PrintTreeRecursive();
		}

		private void Button2_Click( object sender, RoutedEventArgs e )
		{
			var tree = GetDataSet().GenerateDecisionTree();
			DataMiner.Instance.databaseTreeText = tree.PrintTreeRecursive();
		}*/
	}
}
