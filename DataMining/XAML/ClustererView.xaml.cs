﻿using DataMining.DB.BillDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataMining.XAML
{
    /// <summary>
    /// Interaction logic for ClustererView.xaml
    /// </summary>
    public partial class ClustererView : UserControl, INotifyPropertyChanged
    {
        public ClustererView()
        {
            DataContext = this;
            InitializeComponent();
        }
        /**
         *  BINDING boiler-plate
         *  from http://stackoverflow.com/a/1316417
         **/
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public List<Clusterer> Clusterers { get; set; }

        private int _Vlow = 0;
        public int Vlow
        {
            get { return _Vlow; }
            set { SetField(ref _Vlow, value, "Vlow"); }
        }

        private int _Low = 0;
        public int Low
        {
            get { return _Low; }
            set { SetField(ref _Low, value, "Low"); }
        }

        private int _Average = 0;
        public int Average
        {
            get { return _Average; }
            set { SetField(ref _Average, value, "Average"); }
        }

        private int _High = 0;
        public int High
        {
            get { return _High; }
            set { SetField(ref _High, value, "High"); }
        }

        private int _VHigh = 0;
        public int VHigh
        {
            get { return _VHigh; }
            set { SetField(ref _VHigh, value, "VHigh"); }
        }

        private int _ChosenClusterer = 0;
        public int ChosenClusterer
        {
            get { return _ChosenClusterer; }
            set
            {
                SetField(ref _ChosenClusterer, value, "ChosenClusterer");
                Vlow = Clusterers.ElementAt(value).getCostType(0);
                Low = Clusterers.ElementAt(value).getCostType(1);
                Average = Clusterers.ElementAt(value).getCostType(2);
                High = Clusterers.ElementAt(value).getCostType(3);
                VHigh = Clusterers.ElementAt(value).getCostType(4);
            }
        }

        public Clusterer gasDecider { get; set; }
        public Clusterer waterDecider { get; set; }
        public Clusterer elecDecider { get; set; }
        public Clusterer houseDecider { get; set; }
        public Clusterer yearDecider { get; set; }

        public void initialize(Clusterer gasDecider, Clusterer waterDecider, Clusterer elecDecider, Clusterer yearDecider, Clusterer houseDecider)
        {
            List<Clusterer> newClusterers = new List<Clusterer>();
            newClusterers.Add(gasDecider);
            newClusterers.Add(waterDecider);
            newClusterers.Add(elecDecider);
            newClusterers.Add(yearDecider);
            newClusterers.Add(houseDecider);

            Clusterers = newClusterers;

            Vlow = Clusterers.ElementAt(_ChosenClusterer).getCostType(0);
            Low = Clusterers.ElementAt(_ChosenClusterer).getCostType(1);
            Average = Clusterers.ElementAt(_ChosenClusterer).getCostType(2);
            High = Clusterers.ElementAt(_ChosenClusterer).getCostType(3);
            VHigh = Clusterers.ElementAt(_ChosenClusterer).getCostType(4);
        }
    }
}
