﻿using DataMining.Config;
using DataMining.DB.ExampleDB;
using DataMining.DB.HouseDB;
using DataMining.DecisionTree.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataMining.XAML
{
    /// <summary>
    /// Interaction logic for ExampleForm.xaml
    /// </summary>
    public partial class ExampleForm : UserControl, INotifyPropertyChanged
    {
        public ExampleForm()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        /**
        *  BINDING boiler-plate
        *  from http://stackoverflow.com/a/1316417
        **/
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private string _answer = "";
        public string Answer
        {
            get { return _answer; }
            set { SetField(ref _answer, value, "Answer"); }
        }

        private float _size = 12;
        public float Size
        {
            get { return _size; }
            set { SetField(ref _size, value, "Size"); }
        }

        private int _houseType = 2;
        public int HouseType
        {
            get { return _houseType; }
            set { SetField(ref _houseType, value, "HouseType"); }
        }

        private int _heatingType = 3;
        public int HeatingType
        {
            get { return _heatingType; }
            set { SetField(ref _heatingType, value, "HeatingType"); }
        }

        private int _materialType = 1;
        public int MaterialType
        {
            get { return _materialType; }
            set { SetField(ref _materialType, value, "MaterialType"); }
        }

        private int _inhabitantsNumber = 1;
        public int InhabitantsNumber
        {
            get { return _inhabitantsNumber; }
            set { SetField(ref _inhabitantsNumber, value, "InhabitantsNumber"); }
        }

        private int _buildYear;
        public int BuildYear
        {
            get { return _buildYear; }
            set { SetField(ref _buildYear, value, "BuildYear"); }
        }

        private float _averageGasCostPerPerson = 100;
        public float AverageGasCostPerPerson
        {
            get { return _averageGasCostPerPerson; }
            set { SetField(ref _averageGasCostPerPerson, value, "AverageGasCostPerPerson"); }
        }

        private float _averageElectricityCostPerPerson = 100;
        public float AverageElectricityCostPerPerson
        {
            get { return _averageElectricityCostPerPerson; }
            set { SetField(ref _averageElectricityCostPerPerson, value, "AverageElectricityCostPerPerson"); }
        }

        private float _averageWaterCostPerPerson = 100;
        public float AverageWaterCostPerPerson
        {
            get { return _averageWaterCostPerPerson; }
            set { SetField(ref _averageWaterCostPerPerson, value, "AverageWaterCostPerPerson"); }
        }

        private int _gasVendor = 0;
        public int GasVendor
        {
            get { return _gasVendor; }
            set { SetField(ref _gasVendor, value, "GasVendor"); }
        }

        private int _waterVendor = 0;
        public int WaterVendor
        {
            get { return _waterVendor; }
            set { SetField(ref _waterVendor, value, "WaterVendor"); }
        }

        private int _electricityVendor = 0;
        public int ElectricityVendor
        {
            get { return _electricityVendor; }
            set { SetField(ref _electricityVendor, value, "ElectricityVendor"); }
        }

        public ID3Node getFormContent()
        {
            // prepare list of attributes
            List<DecisionTree.Data.Attribute> attributes = new List<DecisionTree.Data.Attribute>();
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.gasDecider.getCluster(AverageGasCostPerPerson).ToString(), Const.AVG_GAS_COST));
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.waterDecider.getCluster(AverageWaterCostPerPerson).ToString(), Const.AVG_WATER_COST));
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.elecDecider.getCluster(AverageElectricityCostPerPerson).ToString(), Const.AVG_ELECTRICITY_COST));
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.yearDecider.getCluster(BuildYear).ToString(), Const.HOUSE_AGE));
            attributes.Add(new DecisionTree.Data.Attribute(((VendorType) ElectricityVendor + 3).ToString(), Const.ELECTRICITY_VENDOR));
            attributes.Add(new DecisionTree.Data.Attribute(((VendorType) GasVendor + 1).ToString(), Const.GAS_VENDOR));
            //attributes.Add(new DecisionTree.Data.Attribute(((VendorType) 10).ToString(), Const.WATER_VENDOR));
            attributes.Add(new DecisionTree.Data.Attribute(DataMiner.Instance.houseDecider.getCluster(Size).ToString(), Const.HOUSE_SIZE));
            attributes.Add(new DecisionTree.Data.Attribute(((MaterialType) MaterialType).ToString(), Const.HOUSE_MATERIAL_TYPE));
            attributes.Add(new DecisionTree.Data.Attribute(InhabitantsNumber.ToString(), Const.HOUSE_INHABITANT_NUMBER));
            attributes.Add(new DecisionTree.Data.Attribute(((HeatingType) HeatingType ).ToString(), Const.HOUSE_HEATING_TYPE));

            return new ID3Node
            {
                Attributes = attributes
            };
        }

        private void GetOpinion(object sender, RoutedEventArgs e)
        {
            getOpinionAsync();
        }

        private delegate void opinionGetter();

        public void getOpinionAsync()
        {
            var main = App.Current.MainWindow as MainWindow;
            main.loading.Visibility = Visibility.Visible;
            main.Everything.IsEnabled = false;

            opinionGetter worker = new opinionGetter(DataMiner.Instance.GenerateTree);
            AsyncCallback completedCallback = new AsyncCallback(MyTaskCompletedCallback);

            AsyncOperation async = AsyncOperationManager.CreateOperation(null);
            worker.BeginInvoke(completedCallback, async);
        }

        private void MyTaskCompletedCallback(IAsyncResult ar)
        {
            // get the original worker delegate and the AsyncOperation instance
            opinionGetter worker =
              (opinionGetter)((AsyncResult)ar).AsyncDelegate;
            AsyncOperation async = (AsyncOperation)ar.AsyncState;

            // finish the asynchronous operation
            worker.EndInvoke(ar);

            // raise the completed event
            AsyncCompletedEventArgs completedArgs = new AsyncCompletedEventArgs(null,
              false, null);
            async.PostOperationCompleted(
              delegate (object e) { OnMyTaskCompleted(); },
              completedArgs);
        }


        protected virtual void OnMyTaskCompleted()
        {
            var instance = getFormContent();
            var output = Tree.ProcessNode(DataMiner.Instance.decisionTree, instance);

            Answer = output.ToString();

            var main = App.Current.MainWindow as MainWindow;
            main.loading.Visibility = Visibility.Hidden;
            main.Everything.IsEnabled = true;

            main.ClustererPanel.initialize
            (
                DataMiner.Instance.gasDecider,
                DataMiner.Instance.waterDecider,
                DataMiner.Instance.elecDecider,
                DataMiner.Instance.yearDecider,
                DataMiner.Instance.houseDecider
            );
        }
    }
}
