﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataMining.Algorithm
{
	public class ProblemClass
	{
		Dictionary<string, float> properties = new Dictionary<string, float>();
		public ProblemClass( string initProperty = "" )
		{
			if ( String.IsNullOrEmpty( initProperty ) )
				RegisterProperty( initProperty );
		}

		public void RegisterProperty( string name )
		{
			if ( !properties.ContainsKey( name ) )
			{
				properties.Add( name, 0.0f );
			}
		}

		public void SetProperty( string name, float p )
		{
			if ( !properties.ContainsKey( name ) )
			{
				RegisterProperty( name );
			}
			properties[ name ] = p;
		}

		public float GetProperty( string name )
		{
            return 0;
		}

		public override string ToString()
		{
			StringBuilder s = new StringBuilder();
			foreach ( var kv in properties )
			{
				s.Append( "  [" + kv.Key + "]: " + kv.Value.ToString() );
			}
			return s.ToString();
		}
    }
}
